��          t      �         	             )     8     ?  2   P  
   �     �  Z   �     �  �       �               3     :  X   W     �     �  n   �     7            
         	                                       All Files Default Color Default Folder Images No file selected Note: you cannot change the color with rox desktop Open Image Options This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper - help Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-12-29 21:07+0200
PO-Revision-Date: 2018-09-23 23:20+0300
Last-Translator: cyril cottet <cyrilusber2001@yahoo.fr>
Language-Team: French (http://www.transifex.com/anticapitalista/antix-development/language/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
 Tous les dossiers Couleur par défaut Dossier par défaut Images Pas de fichier sélectionné Note: vous ne pouvez pas modifier la couleur dans l'environnement de bureau ROX Desktop. Ouvrir Image... Options Application antiX permettant de configurer le fond d'écran dans les gestionnaires de fenêtre préinstallés. Fond d'écran antiX - aide 